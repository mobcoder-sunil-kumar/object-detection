export const drawRect = (detections, ctx, videos, videoss) => {
  // Loop through each prediction
  // let max = detections.reduce(prediction => [prediction['score'],prediction['class']])
  const max = detections
    .filter((val) => val['class'] === 'chair' || val['class'] === 'cell phone')
    .reduce((prev, current) => {
      return prev['score'] > current['score'] ? prev : current;
    }, {})['class'];
  detections.forEach((prediction) => {
    // Extract boxes and classes
    const [x, y, width, height] = prediction['bbox'];
    const text = prediction['class'];
    const score = prediction['score'];
    console.log(prediction, 'hey look it');

    // Set styling
    const color = Math.floor(Math.random() * 16777215).toString(16);
    ctx.strokeStyle = '#' + color;
    ctx.font = '18px Arial';

    // Draw rectangles and text
    ctx.beginPath();
    ctx.fillStyle = '#' + color;
    // ctx.fillText(text, x, y);
    // ctx.fillText(score * 100, x - 10, y - 10);
    // ctx.rect(x, y, width, height);

    // draw image
    const img = new Image();
    img.src = './logo192.png';

    if (text === 'cell phone' && text === max) {
      // let dxx = x - (x % 50) + ((x % 50) > 25 ? 50 : -50)
      // let dyy = y - (y % 50) + ((y % 50) > 25 ? 50 : -50)
      // let dw = width - (width % 50) + 40
      // let dh = height - (height % 50) + 40
      ctx.rect(x, y, width, height);
      ctx.fillText(text, x, y);
      ctx.fillText(score * 100, x - 10, y - 10);
      ctx.drawImage(videos, x, y, width, height);
    }

    if (text === 'chair' && text === max) {
      ctx.rect(x, y, width, height);
      ctx.fillText(text, x, y);
      ctx.fillText(score * 100, x - 10, y - 10);
      ctx.drawImage(videoss, x, y, width, height);
    }
    ctx.stroke();
  });
};
